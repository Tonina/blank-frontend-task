# Blank frontend task

This project is a small application which allows users to search and bookmark their favourite Marvel characters.

## Starting an app

In the project directory, you should run:

### `npm install`

Installs the dependecies in the local node_modules folder.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.



