import React, { Component } from "react";
import "./App.css";
import CharacterList from "./CharacterList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      characters: [],
    };
  }

  render() {
    return (
      <div>
        <CharacterList />
      </div>
    );
  }
}

export default App;
