import React, { Component } from "react";
import CharacterItem from "./CharacterItem";

class CharacterList extends Component {
  state = {
    characters: [],
    inputValue: "",
    data: [],
    bookmarked: [],
  };

  async handleChange() {
    let bookmark = JSON.parse(localStorage.getItem("bookmarked"));
    console.log(bookmark === null);
    if (this.state.inputValue === "") {
      if (bookmark === null) {
        this.setState({
          characters: [],
          bookmarked: [],
        });
      } else {
        this.setState({
          characters: bookmark,
          bookmarked: bookmark,
        });
      }
      console.log(this.state.bookmarked);
      return;
    }

    await fetch(
      "https://gateway.marvel.com:443/v1/public/characters?nameStartsWith=" +
        this.state.inputValue +
        "&apikey=f5ff65b9af503b33f047b768e32c92d6"
    )
      .then((res) => res.json())
      .then((data) => {
        this.setState({ characters: data.data.results });
      });

    //this.setState({ characters: this.state.data.data.results });
    if (bookmark !== null) {
      this.setState({ bookmarked: bookmark });
    }
  }

  charactersOnChange = (event) => {
    this.setState(
      {
        inputValue: event.target.value,
      },
      function () {
        this.handleChange();
      }
    );
  };

  handleBookmarked = (e) => {
    console.log("usao u bookmark");
    let help = [];
    let bookmark = JSON.parse(localStorage.getItem("bookmarked"));
    if (bookmark !== null) {
      for (var j = 0; j < bookmark.length; j++) {
        if (bookmark[j].id !== e.metaData.id) help.push(bookmark[j]); //add just ones that arent the same as new bookmark
      }

      if (!bookmark.some((h) => h.id === e.metaData.id)) {
        console.log(e.metaData.id);
        console.log("here");
        //if it was already in there now remove it from bookmarks
        help.push(e.metaData);
      }
      localStorage.setItem("bookmarked", JSON.stringify(help));
      this.handleChange();
    } else {
      help.push(e.metaData);
      localStorage.setItem("bookmarked", JSON.stringify(help));
      this.handleChange();
    }
  };

  componentDidMount() {
    this.handleChange();
  }

  handleButtonRight = (e) => {
    console.log(e.index);
    let bookmark = JSON.parse(localStorage.getItem("bookmarked"));
    let help = [];
    if (e.index === bookmark.length - 1) return;
    for (let i = 0; i < bookmark.length; i++) {
      if (i === e.index) {
        help.push(bookmark[i + 1]);
        help.push(bookmark[i]);
        i++;
      } else {
        help.push(bookmark[i]);
      }
    }
    localStorage.setItem("bookmarked", JSON.stringify(help));
    this.handleChange();
  };

  handleButtonLeft = (e) => {
    let bookmark = JSON.parse(localStorage.getItem("bookmarked"));
    if (e.index === 0) return;

    let a = bookmark[e.index]; //veci
    let b = bookmark[e.index - 1]; //manji

    bookmark[e.index] = b;
    bookmark[e.index - 1] = a;

    localStorage.setItem("bookmarked", JSON.stringify(bookmark));
    this.handleChange();
  };

  render() {
    return (
      <div className="list-container">
        <label htmlFor="search">Search character by name: </label>
        <input type="text" onChange={this.charactersOnChange}></input>
        <div className="container">
          {this.state.characters.map((c, index) => (
            <CharacterItem
              character={c}
              key={index}
              index={index}
              handleBookmarked={this.handleBookmarked}
              handleButtonRight={this.handleButtonRight}
              handleButtonLeft={this.handleButtonLeft}
              bookmarked={this.state.bookmarked}
              check={this.state.bookmarked.some((e) => e.id === c.id)}
              disableRight={index === this.state.bookmarked.length - 1}
              disableLeft={index === 0}
              disable={this.state.inputValue !== ""}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default CharacterList;
