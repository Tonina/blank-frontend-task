import React, { Component } from "react";

class CharacterItem extends Component {
  render() {
    const metaData = this.props.character;
    const { thumbnail, name } = metaData;
    const { handleBookmarked } = this.props;
    const { handleButtonRight, handleButtonLeft } = this.props;
    const { check, index, disableRight, disableLeft, disable } = this.props;
    return (
      <div
        className="character-container"
        style={{ border: "solid black 1px" }}
      >
        <label className="label-container">
          Bookmark me!
          <input
            type="checkbox"
            onChange={handleBookmarked.bind(this, { metaData })}
            checked={check}
          ></input>
        </label>
        <button
          onClick={handleButtonLeft.bind(this, { index })}
          disabled={disable || disableLeft}
        >
          {" "}
          {"<-"}{" "}
        </button>
        <button
          onClick={handleButtonRight.bind(this, { index })}
          disabled={disable || disableRight}
        >
          {" "}
          {"->"}{" "}
        </button>

        <img
          className="character-thumbnail"
          src={`${thumbnail.path}.${thumbnail.extension}`}
          alt="replacement"
        />
        <h3>{name}</h3>
      </div>
    );
  }
}
export default CharacterItem;
